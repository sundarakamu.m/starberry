 <!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>User Data</title>
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="css/style.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="ajax.js"></script>
</head>
<?php
	ob_start();
 	$api_url = 'https://swapi.dev/api/people/';
	
	$ch = curl_init(); 
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
	curl_setopt($ch, CURLOPT_URL,$api_url); 
	$result=curl_exec($ch); 
	$response_data=json_decode($result, true);
	$data = $response_data['results'];

	//echo "<PRE>";print_r($response_data['results']);exit; 
?>
<div class="container">
	<p id="success"></p>
        <div class="table-wrapper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-6">
						<h2> Details List</b></h2>
					</div>
					 
                </div>
            </div>
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
 						<th>Name</th>
                        <th>Height</th>
                        <th>Mass</th>
						<th>Hair Color</th>
                        <th>Skin Color</th>
						<th>Eye Color</th>
						<th>Birth Year</th>
						<th>Gender</th>
						<th>Plants</th>
						<th>Spaceships</th>
						<th>Vehicles</th> 
						
                    </tr>
                </thead>
				<tbody>
				<?php 
				foreach($data as $k=>$val){
				?>
				 
				<tr id="<?php echo $k?>">				 
					<td><?php echo $val['name']; ?></td>
					<td><?php echo $val['height']; ?></td>
					<td><?php echo $val['mass']; ?> </td>
					<td> <?php echo $val['hair_color']; ?></td>
					<td> <?php echo $val['skin_color']; ?></td>
					<td> <?php echo $val['eye_color']; ?></td>
					<td> <?php echo $val['birth_year']; ?></td>
					<td> <?php echo $val['gender']; ?></td>
					 <?php
						if(!empty($val['homeworld'])) {
					  ?> 
					<td>
 						<a href="#" class="edit1"  data-url="<?php echo $val['homeworld']; ?>">
							<i class="material-icons visibility" data-toggle="tooltip" 
 							
 							title="View">&#xe80b;</i>
						</a> 
                     </td>
					 <?php
						} else {
					  ?>
					  <td> - </td>
					 <?php
						}  
					  ?> 
					  
					  <?php
						if(!empty($val['species'])) {
					  ?> 
					<td>
 						<a href="#" class="edit2" data-toggle="modal" data-url="<?php echo $val['species'][0]; ?>">
							<i class="material-icons visibility" data-toggle="tooltip"
 							
 							title="View">&#xf1c8;</i>
						</a> 
                     </td>
					 <?php
						} else {
					  ?>
					  <td> - </td>
					 <?php
						}  
					  ?> 
					  
					    <?php
						if(!empty($val['vehicles'])) {
					  ?> 
					<td>
 						<a href="#" class="edit3" data-url="<?php echo $val['vehicles'][0]; ?>">
							<i class="material-icons visibility" data-toggle="tooltip" 
 							
 							title="View">&#xe531;</i>
						</a> 
                     </td>
					 <?php
						} else {
					  ?>
					  <td> - </td>
					 <?php
						}  
					  ?> 
					  
					  
					  
					  
					  
					 
				</tr>
				
				<?php 
				}
				
				?>
				 
				</tbody>
			</table>
			
        </div>
    </div>
	 
	<!-- Edit Modal HTML -->
	<div id="editModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form id="update_form">
					<div class="modal-header">						
						<h4 class="modal-title">Plants Details</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">
						<input type="hidden" id="id_p" name="id_p" class="form-control" readonly>					
						<div class="form-group">
							<label>Name</label>
							<input type="text" id="name" name="name" class="form-control" readonly>
						</div>
						<div class="form-group">
							<label>rotation period</label>
							<input type="text" id="rotation_period" name="rotation_period" class="form-control" readonly>
						</div>
						<div class="form-group">
							<label>orbital period</label>
							<input type="text" id="orbital_period" name="orbital_period" class="form-control" readonly>
						</div>
						
						
						<div class="form-group">
							<label>diameter</label>
							<input type="text" id="diameter" name="diameter" class="form-control" readonly>
						</div>
						
						<div class="form-group">
							<label>climate</label>
							<input type="text" id="climate" name="climate" class="form-control" readonly>
						</div>
						
						<div class="form-group">
							<label>gravity</label>
							<input type="text" id="gravity" name="gravity" class="form-control" readonly>
						</div>
						
						<div class="form-group">
							<label>terrain</label>
							<input type="text" id="terrain" name="terrain" class="form-control" readonly>
						</div>
						
						<div class="form-group">
							<label>surface water</label>
							<input type="text" id="surface_water" name="surface_water" class="form-control" readonly>
						</div> 
						<div class="form-group">
							<label>population</label>
							<input type="text" id="population" name="population" class="form-control" readonly>
						</div> 	
						
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
 					</div>
					 
				</form>
			</div>
		</div>
	</div>
	 
	<div id="editModal2" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form id="update_form">
					<div class="modal-header">						
						<h4 class="modal-title">Species Details</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">
						<input type="hidden" id="id_p" name="id_p" class="form-control" readonly>					
						<div class="form-group">
							<label>Name</label>
							<input type="text" id="name1" name="name1" class="form-control" readonly>
						</div>
						<div class="form-group">
							<label>classification</label>
							<input type="text" id="classification" name="classification" class="form-control" readonly>
						</div>
						<div class="form-group">
							<label>designation</label>
							<input type="text" id="designation" name="designation" class="form-control" readonly>
						</div>
						
						<div class="form-group">
							<label>Average Hight</label>
							<input type="text" id="average_height" name="average_height" class="form-control" readonly>
						</div>
						
						<div class="form-group">
							<label>Average lifespan</label>
							<input type="text" id="average_lifespan" name="average_lifespan" class="form-control" readonly>
						</div> 
						 
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
 					</div>
					 
				</form>
			</div>
		</div>
	</div> 
	
	<div id="editModal3" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form id="update_form">
					<div class="modal-header">						
						<h4 class="modal-title">Vehicles Details</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">
						<input type="hidden" id="id_p" name="id_p" class="form-control" readonly>					
						<div class="form-group">
							<label>Name</label>
							<input type="text" id="name2" name="name2" class="form-control" readonly>
						</div>
						<div class="form-group">
							<label>model</label>
							<input type="text" id="model" name="model" class="form-control" readonly>
						</div>
						<div class="form-group">
							<label>manufacturer</label>
							<input type="text" id="manufacturer" name="manufacturer" class="form-control" readonly>
						</div>
						
						<div class="form-group">
							<label>cost in credits</label>
							<input type="text" id="cost_in_credits" name="cost_in_credits" class="form-control" readonly>
						</div>
						
						<div class="form-group">
							<label>max atmosphering speed</label>
							<input type="text" id="max_atmosphering_speed" name="max_atmosphering_speed" class="form-control" readonly>
						</div> 
						
						<div class="form-group">
							<label>vehicle class</label>
							<input type="text" id="vehicle_class" name="vehicle_class" class="form-control" readonly>
						</div>
						
						<div class="form-group">
							<label>consumables</label>
							<input type="text" id="consumables" name="consumables" class="form-control" readonly>
						</div>
						 
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
 					</div>
					 
				</form>
			</div>
		</div>
	</div> 

</body>
</html>                                		                            