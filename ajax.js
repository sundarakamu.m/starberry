   
	$(document).on('click','.edit1',function(e) {		
		var url=$(this).attr("data-url");
		 
		//alert(url);
		$.ajax({
			url: "getdata.php",
			type: "POST",
			cache: false,
			data:{
				type:1,
				url: url
			},
			success: function(dataVal){
					$('#editModal').modal('show');
					$("#name").val(dataVal.name);
					$("#rotation_period").val(dataVal.rotation_period);
					$("#orbital_period").val(dataVal.orbital_period);
					$("#diameter").val(dataVal.diameter);
					$("#gravity").val(dataVal.gravity);
					$("#terrain").val(dataVal.terrain);
					$("#surface_water").val(dataVal.surface_water);
					$("#population").val(dataVal.population);
			}
		});
		 
	});
	
	$(document).on('click','.edit2',function(e) {		
		var url=$(this).attr("data-url");
		 
		//alert(url);
		$.ajax({
			url: "getdata.php",
			type: "POST",
			cache: false,
			data:{
				type:1,
				url: url
			},
			success: function(dataVal){
					$('#editModal2').modal('show');
					$("#name1").val(dataVal.name);
					$("#classification").val(dataVal.classification);
					$("#designation").val(dataVal.designation);
					$("#average_height").val(dataVal.average_height);
					$("#average_lifespan").val(dataVal.average_lifespan);
				 
			}
		});
		 
	});
	
	$(document).on('click','.edit3',function(e) {		
		var url=$(this).attr("data-url");
		 
		//alert(url);
		$.ajax({
			url: "getdata.php",
			type: "POST",
			cache: false,
			data:{
				type:1,
				url: url
			},
			success: function(dataVal){
					$('#editModal3').modal('show');
					$("#name2").val(dataVal.name);
					$("#model").val(dataVal.model);
					$("#manufacturer").val(dataVal.manufacturer);
					$("#cost_in_credits").val(dataVal.cost_in_credits);
					$("#max_atmosphering_speed").val(dataVal.max_atmosphering_speed);
					
					$("#vehicle_class").val(dataVal.vehicle_class);
					$("#consumables").val(dataVal.consumables);
				 
				 
			}
		});
		 
	});
	 